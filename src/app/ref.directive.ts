import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appRef]'
})
// будет помогать искать элементы в шаблоне
export class RefDirective {

  constructor(public containerRef: ViewContainerRef) {

  }

}
