import {NgModule} from "@angular/core";
import {PreloadAllModules, Route, RouterModule, Routes} from "@angular/router";
import {HomePageComponent} from "./components/home-page/home-page.component";

const routes: Routes = [
    {path: '', component: HomePageComponent, pathMatch: 'full'},
    // ленивая загрузка
    {path: 'about', loadChildren: () => import('./about-page/about-page.module').then(m => m.AboutPageModule)}
]
@NgModule({
    // стратегия предзагрузки - будет быстрее
    imports: [RouterModule.forRoot(routes, {
        preloadingStrategy: PreloadAllModules
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {

}