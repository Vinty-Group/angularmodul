import {Component, ComponentFactoryResolver, ViewChild} from '@angular/core';
import {ModalComponent} from "./components/modal/modal.component";
import {RefDirective} from "./ref.directive";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild(RefDirective, {static: false}) refDir: RefDirective;
  constructor(
      private resolver: ComponentFactoryResolver,
      private title: Title,
      private meta: Meta) {
    this.title.setTitle('CEO Title');
  }
    showModal() {
      const modalFactory = this.resolver.resolveComponentFactory(ModalComponent);
      this.refDir.containerRef.clear();

      const component = this.refDir.containerRef.createComponent(modalFactory);
      component.instance.title = 'Dynamic Title';
      component.instance.close.subscribe(() => {
        this.refDir.containerRef.clear();
      })
    }
}
