import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AboutPageComponent} from "../components/about-page/about-page.component";
import {AboutExtraPageComponent} from "../components/about-page/about-extra-page/about-extra-page.component";
import {SharedNodule} from "../shared/shared.nodule";
import {RouterModule} from "@angular/router";


@NgModule({
    declarations: [
        AboutPageComponent,
        AboutExtraPageComponent,
    ],
    imports: [
        CommonModule,
        SharedNodule,
        RouterModule.forChild([
            {path: '', component: AboutPageComponent, children: [
                    {path: 'extra', component: AboutExtraPageComponent}
                ]},
        ])
    ],
    exports: [RouterModule]
})
export class AboutPageModule {
}
