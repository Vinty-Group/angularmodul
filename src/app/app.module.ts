import {NgModule, isDevMode} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HomePageComponent} from './components/home-page/home-page.component';
import {RouterLink, RouterLinkActive, RouterOutlet} from "@angular/router";
import {AppRoutingModule} from "./app-routing.module";
import {SharedNodule} from "./shared/shared.nodule";
import { ModalComponent } from './components/modal/modal.component';
import { RefDirective } from './ref.directive';
import { ServiceWorkerModule } from '@angular/service-worker';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { AnimationComponent } from './components/animation/animation.component';


@NgModule({
    declarations: [
        AppComponent,
        HomePageComponent,
        ModalComponent,
        RefDirective,
        AnimationComponent,

    ],
    imports: [
        BrowserModule,
        RouterLink,
        RouterOutlet,
        RouterLinkActive,
        AppRoutingModule,
        SharedNodule,
        BrowserAnimationsModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
          enabled: !isDevMode(),
          // Register the ServiceWorker as soon as the application is stable
          // or after 30 seconds (whichever comes first).
          registrationStrategy: 'registerWhenStable:30000'
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
