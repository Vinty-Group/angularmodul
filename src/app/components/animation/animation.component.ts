import {Component} from '@angular/core';
import {animate, AnimationEvent, group, keyframes, query, state, style, transition, trigger} from "@angular/animations";

@Component({
    selector: 'app-animation',
    templateUrl: './animation.component.html',
    styleUrls: ['./animation.component.css'],
    animations: [
        trigger('box', [
            state('start', style({background: 'blue'})),
            state('end', style({background: 'red', transform: 'scale(1.2)'})),
            state('special',style({
                background: 'orange',
                transform: 'scale(0.5)',
                borderRadius: '50%'
            })),
            transition('start => end', animate(450)),
            transition('end => start', animate('800ms ease-in-out')),
            transition('special <=> *', [
                group([
                    query('h4', animate(1500, style({
                        fontSize: '0.5rem'
                    }))),
                    style({background: 'green'}), animate('1s',
                        style({background: 'pink'})), animate(750)
                ])
            ]),
            // 'void => *' = :enter
            transition(':enter', [
                // keyframes за 4 секунды делит все 4 стиля (выставляем проценты на каждый фрейм)
                    animate('4s', keyframes([
                        style({background: 'red', offset: 0 }),
                        style({background: 'black', offset: .2}),
                        style({background: 'orange', offset: .5}),
                        style({background: 'blue', offset: 1}),
                    ]))
                    // style({opacity: 0}),
                    // animate('850ms ease-out')
                ,
                // '* => void' = :leave
                transition(':leave', [
                    style({opacity: 1}),
                    animate(750, style({opacity:0, transform: 'scale(1.2)'})),
                    animate(300, style({
                        color: '#000',
                        fontWeight: 'bold'
                    }))
                ])

            ])
        ])
    ]
})
export class AnimationComponent {
    boxState = 'start';
    visible = true;
    myAnimate() {
        this.boxState = this.boxState === 'end' ? 'start' : 'end';
    }

    animationStarted($event: AnimationEvent) {
        console.log('animationStarted', event)
    }

    animationDone($event: AnimationEvent) {
        console.log('animationDone', event)
    }
}
